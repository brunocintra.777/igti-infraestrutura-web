var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('build', function(){
   return gulp.src(['server.js', 'index.js'])
	.pipe(concat('all.js'))
        .pipe(gulp.dest('dist'));
});
